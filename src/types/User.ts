type User = {
  name: {
    title: string
    first: string
    last: string
  }
  email: string
  picture: {
    thumbnail: string
  }
  login: {
    username: string
  }
  location: {
    street: {
      number: number
      name: string
    }
    city: string
    state: string
    postcode: string
  }
  nat: string
  phone: string
  cell: string
}

export default User