/**
 * The maximum number of items in the catalog
 */
export const USER_CATALOG_SIZE = 1000

/**
 * How many users to load in one request
 */
export const USER_BATCH_SIZE = 50

/**
 * Retries for MAX_RETRY_COUNT times if it can't load USER_BATCH_SIZE many uses
 */
export const MAX_RETRY_COUNT = 5

/**
 * Available nationalities in the app
 */
export const availableNationalities = ['CH', 'ES', 'FR', 'GB']