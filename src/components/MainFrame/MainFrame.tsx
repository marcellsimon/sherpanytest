import * as React from "react"
import i18n from '../../utils/i18n'
import Menu from "../Menu/Menu"
import UserDetailsModal from "../UserDetailsModal/UserDetailsModal"
import './MainFrame.css'

/**
 * Component for the basic frame around the application.
 * @component
 */
class MainFrame extends React.Component<{}, {}> {

  render() {
    return (
      <div>
        <div className="header">
          <div className="title">
            <img src="/images/logo.svg" className="logo" />
            {i18n('title')}
          </div>
          <Menu />
        </div>
        <div className="content">
          {this.props.children}
        </div>

        <UserDetailsModal />
      </div>
    )
  }
}

export default MainFrame