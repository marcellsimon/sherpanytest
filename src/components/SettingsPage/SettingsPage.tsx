import React from 'react'
import { connect } from 'react-redux'
import { availableNationalities } from '../../constants'
import { filterNationalities } from '../../redux/actions/userActions'
import { IState, UserThunkDispatch } from '../../redux/store'
import i18n from '../../utils/i18n'
import './SettingsPage.css'

type Props = {
  nationalities: string[]
  filterNationalities: Function
}

/**
 * Component for Settings
 * User can set nationality filter on this page
 * @component
 */
class UserDetailsModal extends React.Component<Props, {}> {


  onChecked(nationality: string) {
    if (this.props.nationalities.includes(nationality)) {
      this.props.filterNationalities(this.props.nationalities.filter(n => n !== nationality))
    } else {
      this.props.filterNationalities([...this.props.nationalities, nationality])
    }
  }

  render() {

    return (
      <div className="settings">
        <h4>{i18n('settings.title')}</h4>

        {
          availableNationalities.map(nationality => (
            <div key={nationality} className="nationality-checkbox-container">
              <input
                type="checkbox"
                className="nationality-checkbox"
                checked={this.props.nationalities.includes(nationality)}
                onClick={() => this.onChecked(nationality)}
              />
              {i18n(`nationality.${nationality}`)}
            </div>
          ))
        }
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch: UserThunkDispatch) => ({
  filterNationalities: (nationalities: string[]) => dispatch(filterNationalities(nationalities))
})

const mapStateToProps = (state: IState) => ({
  nationalities: state.ui.filteredNationalities
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetailsModal)