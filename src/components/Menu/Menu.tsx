import * as React from "react"
import { Link } from "react-router-dom"
import i18n from '../../utils/i18n'
import './Menu.css'

type State = {
  path: string
}

/**
 * Component for the menu
 * @component
 */
class Menu extends React.Component<{}, State> {

  state = {
    path: window.location.pathname
  }

  changePage(path: string) {
    this.setState({ path })
  }

  render() {
    const path = this.state.path

    return (
      <div className="menu">
        <Link to='/' onClick={() => this.changePage('/')} className={"menu-item " + (path === '/' ? 'menu-item-selected' : '')}>
          {i18n('menu.users')}
        </Link>
        <Link to='/settings' onClick={() => this.changePage('/settings')} className={"menu-item " + (path === '/settings' ? 'menu-item-selected' : '')}>
          {i18n('menu.settings')}
        </Link>
      </div>
    )
  }
}

export default Menu