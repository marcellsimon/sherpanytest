import AwesomeDebouncePromise from 'awesome-debounce-promise'
import React from 'react'
import { connect } from 'react-redux'
import { USER_CATALOG_SIZE } from '../../constants'
import { clearMaxRetryReached, fetchBatchUsers, searchUsers } from '../../redux/actions/userActions'
import { IState, UserThunkDispatch } from '../../redux/store'
import User from '../../types/User'
import i18n from '../../utils/i18n'
import UserDetails from '../UserDetails/UserDetails'
import './UserList.css'

type Props = {
	loading: boolean
	isMaxRetriesReached: boolean
	errorMessage: string
	users: User[]
	fetchBatchUsers: () => void
	searchUsers: (text: string) => void
	clearMaxRetryReached: () => void
}

/**
 * Component for the user list with search
 * @component
 */
class UserList extends React.Component<Props, {}> {

	async componentDidMount() {
		this.props.fetchBatchUsers()

		window.addEventListener('scroll', () => this.handleScroll())

		await this.fillPageWithUsers()
	}

	async fillPageWithUsers() {

		let loadedNewData
		do {
			loadedNewData = await this.handleScroll()
		} while (loadedNewData)
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll) // TODO
	}

	/**
	 * 
	 * @return boolean true if it loaded data, false if not
	 */
	async handleScroll(): Promise<boolean> {
		if (!this.props.loading &&
			!this.props.isMaxRetriesReached &&
			!this.props.errorMessage &&
			this.props.users.length < USER_CATALOG_SIZE &&
			(window.innerHeight + window.scrollY) >= document.body.scrollHeight - 2) {
			await this.props.fetchBatchUsers()
			return true
		}
		return false
	}

	async handleSearch(text: string) {
		await this.props.searchUsers(text)
		await this.fillPageWithUsers()
	}

	debouncedSearch = AwesomeDebouncePromise((text: string) => this.handleSearch(text), 500)

	render() {
		return (
			<div className="user-list-content">
				<div className="search-container">
					<input
						className="search"
						onChange={(e) => this.debouncedSearch(e.target.value)}
						placeholder={i18n('userList.search')}
					/>
				</div>

				{
					this.props.users.length === 0 && (
						<div className="empty-container">
							<div className="empty-inner">
								<img src="/images/box.svg" className="empty-icon" /><br />
								{i18n('userList.noResult')}
							</div>
						</div>
					)
				}

				{
					this.props.users.length > 0 && (
						<div className="userContainer">
							{this.props.users.map(user => <UserDetails key={user.email} user={user} />)}
						</div>
					)
				}

				{
					this.props.users.length >= USER_CATALOG_SIZE && (
						<div className="loading-frame">
							<div className="loading-inner">
								<img src="/images/warning.svg" className="loading-icon" />
								{i18n('userList.endOfResults')}
							</div>
						</div>
					)
				}

				{
					this.props.loading && (
						<div className="loading-frame">
							<div className="loading-inner">
								<img src="/images/reload.svg" className="loading-icon rotating" />
								{i18n('userList.loading')}
							</div>
						</div>
					)
				}

				{
					this.props.isMaxRetriesReached && (
						<div className="loading-frame">
							<div className="loading-inner">
								<img src="/images/warning.svg" className="loading-icon" />
								<div
									className="retry"
									onClick={async () => {
										await this.props.clearMaxRetryReached()
										this.fillPageWithUsers()
									}}
								>
									{i18n('userList.maximumRetriesReached')}
								</div>
							</div>
						</div>
					)
				}

			</div>
		)
	}
}

const mapDispatchToProps = (dispatch: UserThunkDispatch) => ({
	fetchBatchUsers: () => dispatch(fetchBatchUsers()),
	searchUsers: (text: string) => dispatch(searchUsers(text)),
	clearMaxRetryReached: () => dispatch(clearMaxRetryReached())
})

const mapStateToProps = (state: IState) => ({
	users: state.ui.visibleUsers.map(id => state.entities.users.byEmail[id]),
	loading: state.ui.loading,
	errorMessage: state.ui.errorMessage,
	isMaxRetriesReached: state.ui.isMaxRetriesReached
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(UserList)