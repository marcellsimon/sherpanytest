import React from 'react'
import { connect } from 'react-redux'
import { hideUserModal } from '../../redux/actions/userActions'
import { IState, UserThunkDispatch } from '../../redux/store'
import User from '../../types/User'
import i18n from '../../utils/i18n'
import './UserDetailsModal.css'

type Props = {
  user: User
  hideUserModal: () => void
}

/**
 * Component for a fully detailed user in modal
 * @component
 */
class UserDetailsModal extends React.Component<Props, {}> {

  render() {
    const user = this.props.user

    if (!user) {
      return null
    }

    return (
      <div className="user-details-modal">
        <div className="user-details-modal-main">

          <div className="user-detail-modal-close" onClick={() => this.props.hideUserModal()}>
            X
          </div>

          <div className="user-details-modal-image">
            <img src={user.picture.thumbnail || '/images/user.svg'} alt="" />
          </div>

          <div className="table">
            <div className="row">
              <div className="col-title">{i18n('userDetails.name')}</div>
              <div className="col-data">{user.name.title} {user.name.first} {user.name.last}</div>
            </div>

            <div className="row">
              <div className="col-title">{i18n('userDetails.nationality')}</div>
              <div className="col-data">{i18n(`nationality.${user.nat}`)}</div>
            </div>

            <div className="row">
              <div className="col-title">{i18n('userDetails.username')}</div>
              <div className="col-data">{user.login.username}</div>
            </div>

            <div className="row">
              <div className="col-title">{i18n('userDetails.email')}</div>
              <div className="col-data">{user.email}</div>
            </div>

            <div className="row">
              <div className="col-title">{i18n('userDetails.phone')}</div>
              <div className="col-data">{user.phone}</div>
            </div>

            <div className="row">
              <div className="col-title">{i18n('userDetails.cell')}</div>
              <div className="col-data">{user.cell}</div>
            </div>

            <div className="row">
              <div className="col-title">{i18n('userDetails.address')}</div>
              <div className="col-data">{user.location.postcode} {user.location.street.number} {user.location.street.name}, {user.location.city}, {user.location.state}</div>
            </div>
          </div>

        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch: UserThunkDispatch) => ({
  hideUserModal: () => dispatch(hideUserModal())
})

const mapStateToProps = (state: IState) => ({
  user: state.entities.users.byEmail[state.ui.selectedUser]
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetailsModal)