import React from 'react'
import { showUserModal } from '../../redux/actions/userActions'
import { dispatch } from '../../redux/store'
import User from '../../types/User'
import './UserDetails.css'

type Props = {
  user: User
}

/**
 * Component for one user's details
 * @component
 */
export default class UserDetails extends React.Component<Props, {}> {

  render() {
    const user = this.props.user

    return (
      <div className="user" onClick={() => dispatch(showUserModal(user))}>
        <div className="user-image">
          <img src={user.picture.thumbnail || '/images/user.svg'} alt="" />
        </div>
        <div className="user-details">
          <div className="user-name">
            {user.name.title} {user.name.first} {user.name.last}
          </div>
          <div className="user-email">
            {user.email}
          </div>
        </div>
      </div>
    )
  }
}