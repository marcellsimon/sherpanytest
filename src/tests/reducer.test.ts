
import { availableNationalities } from "../constants"
import { fetchBatchUsers, userActions } from "../redux/actions/userActions"
import userReducer, { initialState } from "../redux/reducers/userReducer"
const testData = require("./testdata.json")
import * as constants from '../constants'

jest.mock('../utils/request', () => {
  return {
    get: (uri: string, params?: Object) => new Promise((resolve, reject) => {
      resolve(testData)
    })
  }
})

describe('actions', () => {

  it('initializes with the initial state', async () => {
    const reducer = userReducer(undefined, { type: "init" })

    expect(reducer).toEqual(initialState)
  })

  it('sets state to loading when fetching starts', async () => {
    const state = await userReducer(undefined, {
      type: userActions.FETCH_BATCH_USERS_STARTED
    })

    expect(state.ui.loading).toBe(true)
  })

  it('fetches users and adds data to store', async () => {
    const state = await userReducer(undefined, {
      type: userActions.FETCH_BATCH_USERS_SUCCESS,
      users: testData.results
    })

    expect(state.entities.users.allEmails.length).toBeGreaterThan(0)
  })

  it('fetches users only for available nationalities', async () => {
    const state = userReducer(undefined, {
      type: userActions.FETCH_BATCH_USERS_SUCCESS,
      users: testData.results
    })

    const userStore = state.ui.visibleUsers
    for (const email of userStore) {
      const item = state.entities.users.byEmail[email]
      expect(availableNationalities.includes(item.nat)).toBe(true)
    }
  })

  it('fetches users only for selected nationalities', async () => {
    let state = userReducer(undefined, {
      type: userActions.FILTER_NATIONALITIES,
      nationalities: ['GB', 'FR']
    })

    expect(state.ui.filteredNationalities).toHaveLength(2)
    expect(state.ui.filteredNationalities).toEqual(expect.arrayContaining(['GB', 'FR']))

    state = userReducer(state, {
      type: userActions.FETCH_BATCH_USERS_SUCCESS,
      users: testData.results
    })

    const visibleUsers = state.ui.visibleUsers

    for (const email of visibleUsers) {
      const user = state.entities.users.byEmail[email]
      expect(['GB', 'FR'].includes(user.nat)).toBe(true)
    }
  })

  it('filters already fetched users by nationalities', async () => {
    let state = userReducer(undefined, {
      type: userActions.FETCH_BATCH_USERS_SUCCESS,
      users: testData.results
    })

    state = userReducer(state, {
      type: userActions.FILTER_NATIONALITIES,
      nationalities: ['GB', 'FR']
    })

    expect(state.ui.filteredNationalities).toHaveLength(2)
    expect(state.ui.filteredNationalities).toEqual(expect.arrayContaining(['GB', 'FR']))

    const visibleUsers = state.ui.visibleUsers

    for (const email of visibleUsers) {
      const user = state.entities.users.byEmail[email]
      expect(['GB', 'FR'].includes(user.nat)).toBe(true)
    }
  })

  it('filters users by name', async () => {
    let state = userReducer(undefined, {
      type: userActions.FETCH_BATCH_USERS_SUCCESS,
      users: testData.results
    })

    state = userReducer(state, {
      type: userActions.SEARCH_USERS,
      text: "Eric"
    })

    expect(state.ui.searchString).toBe("eric")

    const visibleUsers = state.ui.visibleUsers

    for (const email of visibleUsers) {
      const user = state.entities.users.byEmail[email]
      expect(`${user.name.first} ${user.name.last}`.toLowerCase().includes('eric')).toBe(true)
    }
  })

})