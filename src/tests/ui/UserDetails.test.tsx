import React from 'react'
import { Provider } from 'react-redux'
import renderer from 'react-test-renderer'
import User from '../../types/User'
import { flatten } from '../../utils/flattener'
import UserDetails from '../../components/UserDetails/UserDetails'

describe('UserDetails', () => {
  const user: User = {
    name: {
      title: "Mrs",
      first: "Kristin",
      last: "Washington"
    },
    login: {
      username: "mrkrabs"
    },
    location: {
      street: {
        number: 6985,
        name: "Groveland Terrace"
      },
      city: "Darwin",
      state: "South Australia",
      postcode: "4354"
    },
    email: "kristin.washington@example.com",
    phone: "08-5605-1126",
    cell: "0430-068-903",
    picture: {
      thumbnail: "https://randomuser.me/api/portraits/thumb/women/87.jpg"
    },
    nat: "AU"
  }

  it('matches snapshot', () => {
    const component = renderer.create(
      <UserDetails user={user} />
    )

    const tree = component.toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('renders a user in the details view', () => {

    const component = renderer.create(
      <UserDetails user={user} />
    )

    let flattenedTree = flatten(component.toJSON())
    const treeValues = Object.values(flattenedTree)

    expect(treeValues.includes(user.name.first))
    expect(treeValues.includes(user.name.last))
    expect(treeValues.includes(user.email))
  })
})