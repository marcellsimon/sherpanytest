import React from 'react'
import { Provider } from 'react-redux'
import renderer from 'react-test-renderer'
import configureStore, { MockStoreEnhanced } from 'redux-mock-store'
import UserDetailsModal from '../../components/UserDetailsModal/UserDetailsModal'
import { availableNationalities } from '../../constants'
import { IState } from '../../redux/store'
import User from '../../types/User'
import { flatten } from '../../utils/flattener'

describe('UserDetailsModal', () => {

  const user: User = {
    name: {
      title: "Mrs",
      first: "Kristin",
      last: "Washington"
    },
    login: {
      username: "mrkrabs"
    },
    location: {
      street: {
        number: 6985,
        name: "Groveland Terrace"
      },
      city: "Darwin",
      state: "South Australia",
      postcode: "4354"
    },
    email: "kristin.washington@example.com",
    phone: "08-5605-1126",
    cell: "0430-068-903",
    picture: {
      thumbnail: "https://randomuser.me/api/portraits/thumb/women/87.jpg"
    },
    nat: "AU"
  }

  let store: MockStoreEnhanced<IState, {}>

  beforeEach(() => {
    store = configureStore<IState>([])({
      entities: {
        users: {
          byEmail: {
            [user.email]: user
          },
          allEmails: [user.email]
        }
      },
      ui: {
        loading: false,
        errorMessage: undefined,
        filteredNationalities: availableNationalities,
        isMaxRetriesReached: false,
        searchString: undefined,
        selectedUser: user.email,
        visibleUsers: [user.email]
      }
    })
  })

  it('matches snapshot', () => {
    const component = renderer.create(
      <Provider store={store}>
        <UserDetailsModal />
      </Provider>
    )

    const tree = component.toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('renders a user in the modal', () => {
    const component = renderer.create(
      <Provider store={store}>
        <UserDetailsModal />
      </Provider>
    )

    let flattenedTree = flatten(component.toJSON())
    const treeValues = Object.values(flattenedTree)

    expect(treeValues.includes(user.name.first))
    expect(treeValues.includes(user.name.last))
    expect(treeValues.includes(user.email))
    expect(treeValues.includes(user.phone))
    expect(treeValues.includes(user.location.city))
    expect(treeValues.includes(user.location.street.number))
    expect(treeValues.includes(user.location.street.name))
    expect(treeValues.includes(user.location.state))
  })
})