import React from 'react'
import { Provider } from 'react-redux'
import renderer from 'react-test-renderer'
import configureStore, { MockStoreEnhanced } from 'redux-mock-store'
import SettingsPage from '../../components/SettingsPage/SettingsPage'
import UserList from '../../components/UserList/UserList'
import { availableNationalities } from '../../constants'
import { IState } from '../../redux/store'
import { flatten } from '../../utils/flattener'

describe('SettingsPage', () => {

  let store: MockStoreEnhanced<IState, {}>


  it('matches snapshot', () => {
    store = configureStore<IState>([])({
      entities: {
        users: {
          byEmail: {

          },
          allEmails: []
        }
      },
      ui: {
        loading: false,
        errorMessage: undefined,
        filteredNationalities: ['CH', 'FR'],
        isMaxRetriesReached: false,
        searchString: undefined,
        selectedUser: undefined,
        visibleUsers: []
      }
    })

    const component = renderer.create(
      <Provider store={store}>
        <SettingsPage />
      </Provider>
    )

    const tree = component.toJSON()

    expect(tree).toMatchSnapshot()
  })

  it('renders all available languages', () => {
    store = configureStore<IState>([])({
      entities: {
        users: {
          byEmail: {

          },
          allEmails: []
        }
      },
      ui: {
        loading: false,
        errorMessage: undefined,
        filteredNationalities: ['CH'],
        isMaxRetriesReached: false,
        searchString: undefined,
        selectedUser: undefined,
        visibleUsers: []
      }
    })

    const component = renderer.create(
      <Provider store={store}>
        <UserList />
      </Provider>
    )

    const tree = component.toJSON()

    expect(tree).toMatchSnapshot()

  })
})