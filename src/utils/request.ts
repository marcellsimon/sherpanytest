import axios from 'axios'

/**
 * Loads the baseUrl form environment
 */
export function getBaseUrl() {
  const baseUrl = process.env.baseUrl
  return baseUrl
}

const axiosInstance = axios.create({
  baseURL: getBaseUrl(),
  timeout: 60000
})

/**
 * GET request to the given URI with the given params
 * Return type can be specified with generics
 * @param uri 
 * @param params 
 */
export const get = function get<T>(uri: string, params?: Object): Promise<T> {
  return new Promise((resolve, reject) => {
    axiosInstance.get(uri, {
      params: params
    }).then(result => {
      resolve(result.data)
    }).catch(error => {
      //console.error(error)
      reject(error)
    })
  })
}