type TranslationMap = {
  language: string
  texts: Object
}

/**
 * Available translation files
 * If you want to extend it with other languages, add a new json file to the texts folder
 * and add the related info to this object
 */
const availableTranslations: TranslationMap[] = [
  {
    language: 'en',
    texts: require('./texts/en.json')
  },
  {
    language: 'dev',
    texts: {}
  },
]
const defaultLanguage = 'en'

let appLanguage = localStorage.getItem('language')

if (!appLanguage) {
  localStorage.setItem('language', defaultLanguage)
  appLanguage = defaultLanguage
}

/**
 * Returns the translated text for the given key
 * If it's not present in the json file, it will return the code
 * It also works for any depth, so for example:
 * @example
 * in en.json:
 * {
 *  "menu": {
 *    "users": "Users"
 *  }
 * }
 * Then you can call i18n('menu.users') and it will return "Users"
 * But for i18n('menu.dogs') it will return "menu.dogs" as it can't find it in the json
 * @param key the key of the text in the language json file
 */
const i18n = (key: string) => {
  if (appLanguage === 'dev') {
    return key
  }
  try {
    const translationMap = availableTranslations.find(translationmap => translationmap.language === appLanguage)
    if (translationMap) {
      const text = getValue(translationMap.texts, key)
      if (text) {
        return text
      }
    }

  } catch (error) {
    console.log(error)
  }

  return key
}

/**
 * gets a value from an object from any depth
 * @param object like {"a": { "b": "dogs" }}
 * @param path like "a.b"
 */
function getValue(object: any, path: string) {
  return path.
    replace(/\[/g, '.').
    replace(/\]/g, '').
    split('.').
    reduce((o: any, k: string) => (o || {})[k], object);
}

export default i18n