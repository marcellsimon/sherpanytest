import * as React from "react"
import * as ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import MainFrame from "./components/MainFrame/MainFrame"
import SettingsPage from "./components/SettingsPage/SettingsPage"
import UserList from "./components/UserList/UserList"
import { store } from "./redux/store"

/**
 * The root element of the application
 * It routes the uri-s to components
 */
class Routes extends React.Component<{}, {}> {
  render() {
    return (
      <Router>
        <Provider store={store}>
          <MainFrame>
            <Switch>
              <Route exact path="/" component={UserList} />
              <Route exact path="/settings" component={SettingsPage} />
              <Redirect to="/" />
            </Switch>
          </MainFrame>
        </Provider>
      </Router>
    )
  }
}

ReactDOM.render(
  <Routes />,
  document.getElementById("root")
)