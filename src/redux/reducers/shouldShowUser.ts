import User from "../../types/User"

/**
 * User filter function
 * @param user the user object to check
 * @param searchString text search for firstName+lastName
 * @param nationalities only show users if his nationality is in the list
 */
export default function shouldShowUser(user: User, searchString: string, nationalities: string[]): boolean {
  if (!!searchString && !(`${user.name.first} ${user.name.last}`).toLowerCase().includes(searchString)) {
    return false
  }

  if (!nationalities.includes(user.nat)) {
    return false
  }

  return true
}