import { Reducer } from "react"
import { availableNationalities } from "../../constants"
import {
  FetctBatchUsersFailed,
  FetctBatchUsersSuccess,
  FilterNationalities,
  SearchUsers,
  ShowUserModal,
  UserAction,
  userActions
} from "../actions/userActions"
import { IState } from "../store"
import shouldShowUser from "./shouldShowUser"

export const initialState: IState = {
  entities: {
    users: {
      byEmail: {},
      allEmails: []
    },
  },
  ui: {
    loading: false,
    errorMessage: undefined,
    visibleUsers: [],
    searchString: undefined,
    isMaxRetriesReached: false,
    selectedUser: undefined,
    filteredNationalities: availableNationalities
  }
}

const userReducer: Reducer<IState, UserAction> = (
  state = initialState,
  action
): IState => {
  switch (action.type) {
    case userActions.FETCH_BATCH_USERS_STARTED: {
      return {
        ...state,
        ui: {
          ...state.ui,
          loading: true
        }
      }
    }

    case userActions.FETCH_BATCH_USERS_SUCCESS: {
      const allUsers = state.entities.users.byEmail
      const newUsers = (action as FetctBatchUsersSuccess).users

      for (const user of newUsers) {
        allUsers[user.email] = user
      }

      const allEmails = Object.keys(allUsers)

      let filteredUsers = allEmails
      filteredUsers = allEmails.filter(email => {
        const user = allUsers[email]
        return shouldShowUser(user, state.ui.searchString, state.ui.filteredNationalities)
      })

      return {
        ...state,
        entities: {
          users: {
            byEmail: allUsers,
            allEmails: allEmails
          }
        },
        ui: {
          ...state.ui,
          loading: false,
          visibleUsers: filteredUsers
        }
      }
    }

    case userActions.FETCH_BATCH_USERS_FAILED: {
      return {
        ...state,
        ui: {
          ...state.ui,
          loading: false,
          errorMessage: (action as FetctBatchUsersFailed).errorMessage

        }
      }
    }

    case userActions.FETCH_BATCH_USERS_MAX_RETRY_REACHED: {
      return {
        ...state,
        ui: {
          ...state.ui,
          isMaxRetriesReached: true
        }
      }
    }

    case userActions.CLEAR_MAX_RETRY_REACHED: {
      return {
        ...state,
        ui: {
          ...state.ui,
          isMaxRetriesReached: false
        }
      }
    }

    case userActions.SEARCH_USERS: {
      const text = ((action as SearchUsers).text || '').toLowerCase()
      let filteredUsers = state.entities.users.allEmails
      if (text) {
        filteredUsers = state.entities.users.allEmails.filter(email => {
          const user = state.entities.users.byEmail[email]
          return shouldShowUser(user, text, state.ui.filteredNationalities)
        })
      }

      return {
        ...state,
        ui: {
          ...state.ui,
          loading: false,
          searchString: text,
          visibleUsers: filteredUsers,
          isMaxRetriesReached: false
        }
      }
    }

    case userActions.SHOW_USER_MODAL: {
      const user = (action as ShowUserModal).user

      return {
        ...state,
        ui: {
          ...state.ui,
          selectedUser: user.email
        }
      }
    }

    case userActions.HIDE_USER_MODAL: {
      return {
        ...state,
        ui: {
          ...state.ui,
          selectedUser: undefined
        }
      }
    }

    case userActions.FILTER_NATIONALITIES: {
      const nationalities = (action as FilterNationalities).nationalities
      let filteredUsers = state.entities.users.allEmails
      filteredUsers = state.entities.users.allEmails.filter(email => {
        const user = state.entities.users.byEmail[email]
        return shouldShowUser(user, state.ui.searchString, nationalities)
      })

      return {
        ...state,
        ui: {
          ...state.ui,
          filteredNationalities: nationalities,
          visibleUsers: filteredUsers
        }
      }
    }
  }

  return state
}

export default userReducer