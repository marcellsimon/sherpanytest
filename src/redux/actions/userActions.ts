import { Action, AnyAction } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { MAX_RETRY_COUNT, USER_BATCH_SIZE } from '../../constants'
import User from '../../types/User'
import i18n from '../../utils/i18n'
import { get } from '../../utils/request'
import shouldShowUser from '../reducers/shouldShowUser'
import { IState, store } from '../store'

export const enum userActions {
  FETCH_BATCH_USERS = "FETCH_BATCH_USERS",
  FETCH_BATCH_USERS_STARTED = "FETCH_BATCH_USERS_STARTED",
  FETCH_BATCH_USERS_SUCCESS = "FETCH_BATCH_USERS_SUCCESS",
  FETCH_BATCH_USERS_FAILED = "FETCH_BATCH_USERS_FAILED",
  SEARCH_USERS = "SEARCH_USERS",
  FETCH_BATCH_USERS_MAX_RETRY_REACHED = "FETCH_BATCH_USERS_MAX_RETRY_REACHED",
  SHOW_USER_MODAL = "SHOW_USER_MODAL",
  HIDE_USER_MODAL = "HIDE_USER_MODAL",
  FILTER_NATIONALITIES = "FILTER_NATIONALITIES",
  CLEAR_MAX_RETRY_REACHED = "CLEAR_MAX_RETRY_REACHED"
}

export type FetctBatchUsers = Action<string>

export type FetctBatchUsersStarted = Action<string>

export type FetctBatchUsersSuccess = Action<string> & {
  users: User[]
}

export type FetctBatchUsersFailed = Action<string> & {
  errorMessage: string
}

export type FetctBatchUsersMaxRetryReached = Action<string>

export type SearchUsers = Action<string> & {
  text: string
}

export type ShowUserModal = Action<string> & {
  user: User
}

export type HideUserModal = Action<string>

export type FilterNationalities = Action<string> & {
  nationalities: string[]
}

export type ClearMaxRetryReached = Action<string>

export type UserAction = FetctBatchUsers | FetctBatchUsersStarted | FetctBatchUsersSuccess |
  FetctBatchUsersFailed | SearchUsers | ShowUserModal | HideUserModal | FilterNationalities |
  ClearMaxRetryReached

/**
 * Thunk type to handle custom thunk dispatch
 */
type UserThunk = ThunkAction<Promise<void>, IState, unknown, AnyAction>

/**
 * Fetches a batch of users. Batch size can be changed in src/constants.ts
 * It only loads users with the given nationality
 * If text search is active, it will filter the results and only adds the users
 * to the state that matches the filter.
 * It will stop after constans.MAX_RETRY_COUNT retries to  keep the system from a ban
 */
export const fetchBatchUsers = (): UserThunk => async (dispatch) => {
  dispatch(fetchBatchUsersStarted())
  const uiState = store.getState().ui
  const searchString = uiState.searchString
  const isTextSearching = !!searchString
  try {
    const users: User[] = []
    let tries = 0
    do {
      const { results } = await get<{ results: User[] }>('/', {
        results: isTextSearching ? 5 * USER_BATCH_SIZE : USER_BATCH_SIZE,
        nat: uiState.filteredNationalities.length > 0 ? uiState.filteredNationalities.join(',') : undefined
      })
      const newUsers = results.filter(user => shouldShowUser(user, searchString, uiState.filteredNationalities))
      users.push(...newUsers)

      tries++
    } while (tries <= MAX_RETRY_COUNT && users.length < (isTextSearching ? 5 : USER_BATCH_SIZE))
    if (tries > MAX_RETRY_COUNT) {
      dispatch(fetchBatchUsersMaxRetryReached())
    }

    // slice it to be sure that only USER_BATCH_SIZE or less users will be sent
    dispatch(fetchBatchUsersSuccess(users.slice(0, USER_BATCH_SIZE)))
  } catch (error) {
    console.log(error.stack)
    dispatch(fetchBatchUsersFailed(i18n('userList.errorFetching')))
  }
}

/**
 * Indicates that a fetch has been started. Should set state to loading
 */
export const fetchBatchUsersStarted = (): FetctBatchUsersStarted => ({
  type: userActions.FETCH_BATCH_USERS_STARTED
})

/**
 * Action fired after fetching is complete
 * @param users (User[]) the users loaded from the api
 */
export const fetchBatchUsersSuccess = (users: User[]): FetctBatchUsersSuccess => ({
  type: userActions.FETCH_BATCH_USERS_SUCCESS,
  users
})

/**
 * Action fired if fetch has retried for constants.MAX_RETRY_COUNT times but was not able to 
 * get constants.USER_BATCH_SIZE users
 */
export const fetchBatchUsersMaxRetryReached = (): FetctBatchUsersMaxRetryReached => ({
  type: userActions.FETCH_BATCH_USERS_MAX_RETRY_REACHED
})

/**
 * Action fired to clear max retried state
 */
export const clearMaxRetryReached = (): ClearMaxRetryReached => ({
  type: userActions.CLEAR_MAX_RETRY_REACHED
})

/**
 * Action fired if fetch was unsuccesful
 * @param errorMessage (string) the message to be shown
 */
export const fetchBatchUsersFailed = (errorMessage: string): FetctBatchUsersFailed => ({
  type: userActions.FETCH_BATCH_USERS_FAILED,
  errorMessage
})

/**
 * Action to filter user list
 * @param text (string) the text to search for. It will search in firstName+lastName
 */
export const searchUsers = (text: string): SearchUsers => ({
  type: userActions.SEARCH_USERS,
  text
})

/**
 * Action to fire to show the detail modal for the user object
 * @param user (User) The user object to show the modal to
 */
export const showUserModal = (user: User): ShowUserModal => ({
  type: userActions.SHOW_USER_MODAL,
  user
})

/**
 * Action to fire to hide the visible user details modal
 */
export const hideUserModal = (): HideUserModal => ({
  type: userActions.HIDE_USER_MODAL
})

/**
 * Action to fire if the filter for nationalities change
 * @param nationalities (string[]) the nationalities to filter users
 */
export const filterNationalities = (nationalities: string[]): FilterNationalities => ({
  type: userActions.FILTER_NATIONALITIES,
  nationalities
})

