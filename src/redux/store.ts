import { applyMiddleware, createStore, Store as ReduxStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk, { ThunkDispatch } from 'redux-thunk'
import User from '../types/User'
import { UserAction } from './actions/userActions'
import userReducer from './reducers/userReducer'

export type UserThunkDispatch = ThunkDispatch<IState, void, UserAction>

export interface IState {
  entities: {
    users: {
      byEmail: {
        [key: string]: User
      },
      allEmails: string[]
    },
  },
  ui: {
    loading: boolean
    errorMessage: string
    visibleUsers: string[]
    searchString: string
    isMaxRetriesReached: boolean
    selectedUser: string
    filteredNationalities: string[]
  }
}

export type UserStore = ReduxStore<IState, UserAction>

export function createUserStore() {
  const store: UserStore = createStore(userReducer, composeWithDevTools(applyMiddleware(thunk)))
  const dispatch = store.dispatch as UserThunkDispatch
  const getState = store.getState()

  return {
    store,
    dispatch,
    getState
  }
}

export const { store, dispatch, getState } = createUserStore()