const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

var path = require('path')

var BUILD_DIR = path.resolve(__dirname, '../dist')
var APP_DIR = path.resolve(__dirname, '../src')

console.log(APP_DIR)

module.exports = {
  entry: {
    app: APP_DIR + '/Routes.tsx'
  },

  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      maxSize: 1024 * 512,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name() {
            // put node_modules in a separate folder
            return `js/vendor`
          },
        },
      },
    },
  },

  devServer: {
    compress: true,
    port: 4000,
    disableHostCheck: true
  },

  output: {
    path: BUILD_DIR,
    filename: '[name].[contenthash].js',
    publicPath: '/'
  },

  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json", '.scss', '.css']
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: { allowTsInNodeModules: true }
      },
      {
        test: /\.less$/,
        loader: 'less-loader' // compiles Less to CSS
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        { from: 'src/images', to: 'images' }
      ]
    }),
    new HtmlWebpackPlugin({ template: path.resolve(__dirname, '../src/index.html') }),
  ]

};