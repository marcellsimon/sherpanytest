const webpack = require('webpack')
const { merge } = require('webpack-merge')
const baseConfig = require('./base.config.js')

module.exports = merge(baseConfig, {

  // we have to redefine the output for local, because [contenthash] is not available
  output: {
    filename: '[name].[hash].js'
  },

  devServer: {
    compress: true,
    port: 4004,
    disableHostCheck: true
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      baseUrl: "https://randomuser.me/api"
    }),
  ]

})