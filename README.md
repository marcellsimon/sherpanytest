  

# SherpanyTest

  

**To start the project**

  

```
npm install
npm start
```
  

App will run on http://localhost:4004/

  

**To run tests**

```
npm test
```

  

If tests fail because of snapshots, and you are sure that the changes are fine, run:

```
npm run test-update-snapshot
```